import {Component, Injectable, OnInit} from '@angular/core';
import {Article, DemoService, Order} from "./demo.service";
import {Globals} from "./globals";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

@Injectable({
  providedIn: 'root'
})
export class AppComponent implements OnInit{

  constructor(private service: DemoService, public globals: Globals) { }

  ngOnInit(): void {
    this.refresh();
  }

  articles: Article[] = [];
  orders: Order[] = [];

  refresh() {
    this.getArticles();
    this.getOrders();
    this.globals.message = undefined;
  }

  getArticles() {
    this.service.getArticles().subscribe((data) => {
      this.articles = data;
    });
  }

  getOrders() {
    this.service.orders().subscribe((data) => {
      this.orders = data;
    });
  }

  place(code: string, amount: number) {
    this.service.place(code, amount).subscribe((order) => {
      this.refresh();
    });
  }

  clear() {
    this.service.clearAll().subscribe(() => {
      this.refresh();
    });
  }

  buy(code: string) {
    this.service.buy(code).subscribe((order) => {
      this.refresh();
    });
  }


}
