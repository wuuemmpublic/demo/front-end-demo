import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DemoService {

  constructor(private http: HttpClient) {

  }

  url: string = "http://127.0.0.1:5555/"

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.url + "articles");
  }

  clearAll(): Observable<Order[]> {
    return this.http.get<Order[]>(this.url + "clear");
  }

  place(code: string, amount: number): Observable<Order> {
    return this.http.get<Order>(this.url + code + "/" + amount + "/place");
  }

  buy(code: string): Observable<Order> {
    return this.http.get<Order>(this.url + code + "/buy");
  }

  orders(): Observable<Order[]> {
    return this.http.get<Order[]>(this.url);
  }

}

export class Article {
  code: string = "";
  name: string = "";
  stock: number = 0;
  available: number = 0;
  amount: number = 0;
}

export class Order {
  code: string = "";
  amount: number = 0;
  status: string = "";
  articleId: string = "";
}
