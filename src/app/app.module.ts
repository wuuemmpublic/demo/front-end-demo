import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import {DemoService} from "./demo.service";
import {FormsModule} from "@angular/forms";
import {ErrorInterceptor} from "./interceptor";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {Globals} from "./globals";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    DemoService,
    Globals,
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
