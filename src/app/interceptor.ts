import {Injectable} from "@angular/core";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {catchError} from "rxjs/operators";
import {Globals} from "./globals";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private globals: Globals) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const clonedRequest = req.clone({
      withCredentials: true,
    });

    return next.handle(clonedRequest).pipe(
      catchError((err: any) => {
        if(err instanceof HttpErrorResponse) {
          console.warn("Error Data", err.error);
          const regx= /.*?\[(.*?)]/g;
          const match = regx.exec(err.error.message);
          if (match) {
            const ob = JSON.parse(match[1]);
            this.globals.message = ob.message;
          } else {
            this.globals.message = "Wystąpił błąd serwera";
          }
        }
        return of(err);
      }));
  }

}
