FROM node:14.18.1 as build-step
RUN mkdir -p /app
WORKDIR /app
COPY . /app
RUN npm install
RUN npm run build --prod

FROM nginx:1.21.3
COPY --from=build-step /app/dist/demo-front /usr/share/nginx/html
